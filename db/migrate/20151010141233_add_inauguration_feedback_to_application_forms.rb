class AddInaugurationFeedbackToApplicationForms < ActiveRecord::Migration
  def change
    add_column :application_forms, :inauguration_feedback, :text
  end
end
