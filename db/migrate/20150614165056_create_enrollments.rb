class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.references :application_form, index: true, foreign_key: true
      t.references :project, index: true, foreign_key: true
      t.boolean :team_leader, default: false
      t.boolean :spin_off, default: false
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
