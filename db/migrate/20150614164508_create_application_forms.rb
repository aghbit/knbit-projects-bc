class CreateApplicationForms < ActiveRecord::Migration
  def change
    create_table :application_forms do |t|
      t.integer :user_id
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
