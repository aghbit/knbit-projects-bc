class TimeSlot < ActiveRecord::Base
  belongs_to :application_form

  validates :start_time, :end_time, presence: true
  validates :start_time, :end_time, uniqueness: true
  validates :application_form_id, uniqueness: true, if: Proc.new { |ts| ts.application_form_id.present? }

  def self.extract_slots(start_time, end_time, duration = 15.minutes)
    slots = []
    time = start_time
    while time < end_time
      slot_start = time
      slot_end = (time += duration)
      slots << {start_time: slot_start, end_time: slot_end}
    end

    slots
  end

  def free?
    application_form.nil?
  end

  def taken?
    application_form.present?
  end
end
