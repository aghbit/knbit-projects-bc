class ApplicationForm < ActiveRecord::Base
  attr_accessor :time_slot_id

  enum status: {pending: 0, resolved: 1}

  has_many :enrollments, dependent: :destroy
  has_one :meeting_time, class: TimeSlot, dependent: :nullify

  validates :user_id, presence: true
  validate :application_already_submitted, on: :create
  validate :meeting_time_available?

  accepts_nested_attributes_for :enrollments

  def set_meeting(time_slot)
    self.meeting_time = time_slot if time_slot.free?
  end

  def assign_meeting
    return unless time_slot_id.present?

    set_meeting(TimeSlot.find(time_slot_id))
  end

  def user(user_store = nil)
    @user ||= user_store.present? ? user_store.fetch(user_id) : UserService.fetch(user_id)
  end

  private

  # TODO extract to some sexy custom validator for enums
  def application_already_submitted
    if self.class.pending.where(user_id: user_id).present?
      errors.add(:user_id, 'application already submitted')
    end
  end

  def meeting_time_available?
    return if time_slot_id.blank? || time_slot_id.to_i == meeting_time.try(:id)

    if TimeSlot.find(time_slot_id).taken?
      errors.add(:time_slot_id, 'time slot already taken')
    end
  end
end
