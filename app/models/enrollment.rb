class Enrollment < ActiveRecord::Base
  belongs_to :application_form
  belongs_to :project

  delegate :user, to: :application_form

  def accept!
    role = team_leader ? :team_leader : :developer
    Membership.create(project_id: project_id, user_id: application_form.user_id, role: role)

    update_attributes(accepted: true)
  end

  def reject!
    Membership.where(project_id: project_id, user_id: application_form.user_id).first!.destroy

    update_attributes(accepted: false)
  end
end
