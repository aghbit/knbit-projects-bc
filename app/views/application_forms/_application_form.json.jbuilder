json.id application_form.id
json.user_id application_form.user.id
json.user do
  json.id application_form.user.id
  json.first_name application_form.user.first_name
  json.last_name application_form.user.last_name
  json.email application_form.user.email
  json.department_name application_form.user.department_name
  json.index_number application_form.user.index_number
  json.start_of_studies_year application_form.user.start_of_studies_year
end
json.status application_form.status
json.notes application_form.notes
json.inauguration_feedback application_form.inauguration_feedback
json.created_at application_form.created_at
json.enrollments application_form.enrollments, partial: 'enrollments/enrollment', as: :enrollment
if application_form.meeting_time.present?
  json.meeting_time do json.partial! application_form.meeting_time end
else
  json.meeting_time nil
end