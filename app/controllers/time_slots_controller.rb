class TimeSlotsController < ApplicationController

  before_action :set_time_slot, only: [:update, :destroy]
  before_action :authorize_admin, only: [:create, :create_day, :update, :destroy]

  def index
    @time_slots = TimeSlot.all
  end

  def show
  end

  def create
    begin
      @time_slot = TimeSlot.create!(time_slot_params)
      status = :ok
    rescue ActiveRecord::RecordInvalid
      status = :unprocessable_entity
    end

    render :show, status: status
  end

  def create_day
    start_time, end_time = params[:time_slots].values_at(:start_time, :end_time).map(&:to_time)
    time_slots_params = TimeSlot.extract_slots(start_time, end_time)

    @time_slots = time_slots_params.map do |time_slot|
      TimeSlot.create(time_slot) unless TimeSlot.exists?(time_slot)
    end.compact

    render :index, status: status
  end

  def update
    if @time_slot.update(time_slot_params)
      render :show, status: :ok
    else
      render json: {errors: @time_slot.errors.messages}, status: :unprocessable_entity
    end
  end

  def destroy
    @time_slot.destroy

    head :ok
  end

  private

  def set_time_slot
    @time_slot = TimeSlot.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def time_slot_params
    params.require(:time_slot).permit(:start_time, :end_time)
  end
end