class EnrollmentsController < ApplicationController
  before_action :set_application_form
  before_action :set_enrollment
  before_action :authorize_admin

  def accept
    @enrollment.accept!
    head :ok
  rescue ActiveRecord::RecordInvalid
    head :unprocessable_entity
  end

  def reject
    @enrollment.reject!
    head :ok
  rescue ActiveRecord::RecordInvalid
    head :unprocessable_entity
  end

  private

  def set_application_form
    @application_form = ApplicationForm.find(params[:application_form_id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end

  def set_enrollment
    @enrollment = @application_form.enrollments.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end
end
