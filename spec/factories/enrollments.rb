FactoryGirl.define do
  factory :enrollment do
    application_form
    project
    team_leader false
    spin_off false
    accepted false
  end
end
