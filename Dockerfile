FROM ruby:2.2.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN mkdir /app
WORKDIR /app

ADD Gemfile ./Gemfile
ADD Gemfile.lock ./Gemfile.lock
RUN gem install bundler && bundle install --jobs 20 --retry 5

ADD . ./

EXPOSE 3000
